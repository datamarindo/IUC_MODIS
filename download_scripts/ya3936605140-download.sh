#!/bin/bash

GREP_OPTIONS=''

cookiejar=$(mktemp cookies.XXXXXXXXXX)
netrc=$(mktemp netrc.XXXXXXXXXX)
chmod 0600 "$cookiejar" "$netrc"
function finish {
  rm -rf "$cookiejar" "$netrc"
}

trap finish EXIT
WGETRC="$wgetrc"

prompt_credentials() {
    echo "Enter your Earthdata Login or other provider supplied credentials"
    read -p "Username (eglagunes): " username
    username=${username:-eglagunes}
    read -s -p "Password: " password
    echo "machine urs.earthdata.nasa.gov login $username password $password" >> $netrc
    echo
}

exit_with_error() {
    echo
    echo "Unable to Retrieve Data"
    echo
    echo $1
    echo
    echo "https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2022.01.25/MOD11A2.A2022025.h08v07.061.2022035033020.hdf"
    echo
    exit 1
}

prompt_credentials
  detect_app_approval() {
    approved=`curl -s -b "$cookiejar" -c "$cookiejar" -L --max-redirs 5 --netrc-file "$netrc" https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2022.01.25/MOD11A2.A2022025.h08v07.061.2022035033020.hdf -w %{http_code} | tail  -1`
    if [ "$approved" -ne "302" ]; then
        # User didn't approve the app. Direct users to approve the app in URS
        exit_with_error "Please ensure that you have authorized the remote application by visiting the link below "
    fi
}

setup_auth_curl() {
    # Firstly, check if it require URS authentication
    status=$(curl -s -z "$(date)" -w %{http_code} https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2022.01.25/MOD11A2.A2022025.h08v07.061.2022035033020.hdf | tail -1)
    if [[ "$status" -ne "200" && "$status" -ne "304" ]]; then
        # URS authentication is required. Now further check if the application/remote service is approved.
        detect_app_approval
    fi
}

setup_auth_wget() {
    # The safest way to auth via curl is netrc. Note: there's no checking or feedback
    # if login is unsuccessful
    touch ~/.netrc
    chmod 0600 ~/.netrc
    credentials=$(grep 'machine urs.earthdata.nasa.gov' ~/.netrc)
    if [ -z "$credentials" ]; then
        cat "$netrc" >> ~/.netrc
    fi
}

fetch_urls() {
  if command -v curl >/dev/null 2>&1; then
      setup_auth_curl
      while read -r line; do
        # Get everything after the last '/'
        filename="${line##*/}"

        # Strip everything after '?'
        stripped_query_params="${filename%%\?*}"

        curl -f -b "$cookiejar" -c "$cookiejar" -L --netrc-file "$netrc" -g -o $stripped_query_params -- $line && echo || exit_with_error "Command failed with error. Please retrieve the data manually."
      done;
  elif command -v wget >/dev/null 2>&1; then
      # We can't use wget to poke provider server to get info whether or not URS was integrated without download at least one of the files.
      echo
      echo "WARNING: Can't find curl, use wget instead."
      echo "WARNING: Script may not correctly identify Earthdata Login integrations."
      echo
      setup_auth_wget
      while read -r line; do
        # Get everything after the last '/'
        filename="${line##*/}"

        # Strip everything after '?'
        stripped_query_params="${filename%%\?*}"

        wget --load-cookies "$cookiejar" --save-cookies "$cookiejar" --output-document $stripped_query_params --keep-session-cookies -- $line && echo || exit_with_error "Command failed with error. Please retrieve the data manually."
      done;
  else
      exit_with_error "Error: Could not find a command-line downloader.  Please install curl or wget"
  fi
}

fetch_urls <<'EDSCEOF'
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2022.01.25/MOD11A2.A2022025.h08v07.061.2022035033020.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2022.01.17/MOD11A2.A2022017.h08v07.061.2022028034223.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2022.01.09/MOD11A2.A2022009.h08v07.061.2022018195520.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2022.01.01/MOD11A2.A2022001.h08v07.061.2022010045328.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.12.27/MOD11A2.A2021361.h08v07.061.2022005043837.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.12.19/MOD11A2.A2021353.h08v07.061.2022003215710.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.12.11/MOD11A2.A2021345.h08v07.061.2021355030345.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.12.03/MOD11A2.A2021337.h08v07.061.2021346054009.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.11.25/MOD11A2.A2021329.h08v07.061.2021338210807.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.11.17/MOD11A2.A2021321.h08v07.061.2021331200923.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.11.09/MOD11A2.A2021313.h08v07.061.2021322065725.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.11.01/MOD11A2.A2021305.h08v07.061.2021314073035.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.10.24/MOD11A2.A2021297.h08v07.061.2021306213130.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.10.16/MOD11A2.A2021289.h08v07.061.2021299102734.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.10.08/MOD11A2.A2021281.h08v07.061.2021290050434.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.09.30/MOD11A2.A2021273.h08v07.061.2021282050201.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.09.22/MOD11A2.A2021265.h08v07.061.2021316132222.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.09.14/MOD11A2.A2021257.h08v07.061.2021266041606.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.09.06/MOD11A2.A2021249.h08v07.061.2021258063306.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.08.29/MOD11A2.A2021241.h08v07.061.2021251172112.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.08.21/MOD11A2.A2021233.h08v07.061.2021243235715.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.08.13/MOD11A2.A2021225.h08v07.061.2021234064523.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.08.05/MOD11A2.A2021217.h08v07.061.2021226062851.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.07.28/MOD11A2.A2021209.h08v07.061.2021218050419.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.07.20/MOD11A2.A2021201.h08v07.061.2021214022644.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.07.12/MOD11A2.A2021193.h08v07.061.2021202214500.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.07.04/MOD11A2.A2021185.h08v07.061.2021194053300.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.06.26/MOD11A2.A2021177.h08v07.061.2021188184329.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.06.18/MOD11A2.A2021169.h08v07.061.2021178062847.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.06.10/MOD11A2.A2021161.h08v07.061.2021170050743.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.06.02/MOD11A2.A2021153.h08v07.061.2021162055656.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.05.25/MOD11A2.A2021145.h08v07.061.2021154055217.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.05.17/MOD11A2.A2021137.h08v07.061.2021146123703.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.05.09/MOD11A2.A2021129.h08v07.061.2021138051927.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.05.01/MOD11A2.A2021121.h08v07.061.2021133170007.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.04.23/MOD11A2.A2021113.h08v07.061.2021122122038.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.04.15/MOD11A2.A2021105.h08v07.061.2021114054255.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.04.07/MOD11A2.A2021097.h08v07.061.2021107043800.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.03.30/MOD11A2.A2021089.h08v07.061.2021098043405.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.03.22/MOD11A2.A2021081.h08v07.061.2021090060232.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.03.14/MOD11A2.A2021073.h08v07.061.2021082170804.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.03.06/MOD11A2.A2021065.h08v07.061.2021075154012.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.02.26/MOD11A2.A2021057.h08v07.061.2021068144113.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.02.18/MOD11A2.A2021049.h08v07.061.2021062063740.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.02.10/MOD11A2.A2021041.h08v07.061.2021050033732.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.02.02/MOD11A2.A2021033.h08v07.061.2021042232120.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.01.25/MOD11A2.A2021025.h08v07.061.2021042222218.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.01.17/MOD11A2.A2021017.h08v07.061.2021041194702.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.01.09/MOD11A2.A2021009.h08v07.061.2021041052844.hdf
https://e4ftl01.cr.usgs.gov//DP131/MOLT/MOD11A2.061/2021.01.01/MOD11A2.A2021001.h08v07.061.2021040211255.hdf
EDSCEOF