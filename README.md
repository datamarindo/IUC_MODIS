# Isla Urbana de Calor a partir de imágenes MODIS

![isla urbana de calor](t_periodo.png)

Para describir la IUC de la zona de estudio se utilizan imágenes del proyecto MODIS, del satélite Terra, para todo el periodo disponible. Se utiliza el producto promedio de 8 días a una resolución de 1 km, con clave de catálogo MOD11A2 V 061, disponibles desde el portal de search.earthdatasa.gov.

Las áreas urbanas de la zona de estudio se clasifican manualmente en previas y posteriores a 2010, utilizando imágenes satelitales de alta resolución (Bing y Google); la zona rural para comparar las urbes se puede trazar a partir de un radio de 10 km con respecto a las zonas urbanas identificadas. Es importante retirar de los polígonos del área de estudio los cuerpos de agua grandes, porque funcionan como reservorios de calor.

Las imágenes se procesan con el software R (R Core Team 2021), para determinar el comportamiento espaciotemporal de la IUC, utilizando las librerías terra (Hijmans 2021) para procesos geoespaciales, exactextractr (Daniel Baston 2021) para estadísticas zonales y ggseas (Ellis 2018) para descomposición de series de tiempo.

![serie de tiempo](t_trend.png)

Si bien el análisis se hizo para la ciudad de Veracruz, las imágenes de MODIS de los links de descarga abarcan un área importante que incluye desde Jalisco hasta Veracruz y hasta Oaxaca hacia el sur, inclyendo las ciudades de Puebla, CDMX, Xalapa, Oaxaca, Cuernavaca, Tlaxcala, Morelia, Acapulco y Chilpancingo.

![extensión de la imagen](extension_imagenes.png)


